package main

import (
	"database/sql"
	"fmt"
	"io/ioutil"
	"log"
	"strings"
	"time"
	"sync"
	tgbotapi "github.com/go-telegram-bot-api/telegram-bot-api"
	_ "github.com/mattn/go-sqlite3"
	"github.com/skip2/go-qrcode"
	"os"
	"unicode"
    "image/color"
	"image"
    "image/draw"
    "github.com/fogleman/gg"
)

var dbMutex sync.Mutex

var milk string
var coffee string
var syrup string
var ice string
var selectedUserID int64
var selectedUserName string
var keyboard = tgbotapi.NewReplyKeyboard(
	tgbotapi.NewKeyboardButtonRow(
		tgbotapi.NewKeyboardButton("My QR code"),
		tgbotapi.NewKeyboardButton("I'm going for a coffee"),
	),
	tgbotapi.NewKeyboardButtonRow(
		tgbotapi.NewKeyboardButton("How long do I have to wait"),
		tgbotapi.NewKeyboardButton("Statistics"),
	),
	tgbotapi.NewKeyboardButtonRow(
		tgbotapi.NewKeyboardButton("Update Pret Number"),
		tgbotapi.NewKeyboardButton("Share Subscription Code"),
	),
	tgbotapi.NewKeyboardButtonRow(
		tgbotapi.NewKeyboardButton("Ask for coffee"),
	),
)

func main() {
	botToken, err := ioutil.ReadFile("./config/token.txt")
	if err != nil {
		log.Println(err)
		return
	}
	bot, err := tgbotapi.NewBotAPI(strings.TrimSpace(string(botToken)))
	if err != nil {
		log.Println(err)
		return
	}

	db, err := sql.Open("sqlite3", "./mydb.db")
	if err != nil {
		log.Println(err)
		return
	}
	defer db.Close()

	_, err = db.Exec(`CREATE TABLE IF NOT EXISTS users (userID INTEGER PRIMARY KEY, userName TEXT, cardID TEXT, coffeeTotal INTEGER, lastNotification INTEGER)`)
	

	if err != nil {
		log.Println(err)
		return
	}

	_, err = db.Exec(`CREATE TABLE IF NOT EXISTS cards (cardID TEXT PRIMARY KEY, lastDateTime INTEGER, coffeeAmount INTEGER, sentMessage INTEGER, lastdate DATE)`)

	if err != nil {
		log.Println(err)
		return
	}

	confirmationKeyboard := tgbotapi.NewReplyKeyboard(
		tgbotapi.NewKeyboardButtonRow(
			tgbotapi.NewKeyboardButton("Yes, please"),
			tgbotapi.NewKeyboardButton("No, take me back"),
		),
	)

	inlineBtn := tgbotapi.NewInlineKeyboardButtonData("I got a coffee", "coffeereceived")
	inlineKeyboard := tgbotapi.NewInlineKeyboardMarkup(tgbotapi.NewInlineKeyboardRow(inlineBtn))

	go func() {
		for {
			time.Sleep(30 * time.Second) // Check every 30 seconds

			var cardIDs []string
			currentTime := time.Now().Unix()

			rows, err := db.Query("SELECT cardID, lastDateTime FROM cards WHERE (sentMessage = 0 AND ? - lastDateTime >= 1800)", currentTime)

			if err != nil {
				log.Println(err)
				return
			}
			for rows.Next() {
				var cardID string
				var lastDateTime int64
				err := rows.Scan(&cardID, &lastDateTime)
				if err != nil {
					log.Println(err)
					return
				}
				cardIDs = append(cardIDs, cardID)
			}
			rows.Close()

			for _, cardID := range cardIDs {
				notifyOtherUsers(bot, cardID, db, keyboard)
				_, err := db.Exec("UPDATE cards SET sentMessage = 1 WHERE cardID = ?", cardID)
				if err != nil {
					log.Println(err)
					return
				}
			}
		}
	}()


	u := tgbotapi.NewUpdate(0)
	u.Timeout = 60

	updates, err := bot.GetUpdatesChan(u)
	if err != nil {
		log.Println(err)
		return
	}

	for update := range updates {
		if update.Message != nil {

			userID := update.Message.Chat.ID
			var cardID string

			if strings.HasPrefix(update.Message.Text, "/start") {
				// Check if the message contains additional parameters
				if strings.Contains(update.Message.Text, " ") {
					params := strings.Split(update.Message.Text, " ")
					if len(params) > 1 {
						cardID = params[1]
						// Check if the user exists in the database
						var count int
						err = db.QueryRow("SELECT COUNT(*) FROM users WHERE userID = ?", userID).Scan(&count)
						if err != nil {
							log.Println(err)
							return
						}
						if count == 0 {
							// User does not exist, insert a new record
							firstName := strings.TrimSpace(update.Message.Chat.FirstName)
							lastName := strings.TrimSpace(update.Message.Chat.LastName)
							userName := firstName
							if lastName != "" {
								userName = fmt.Sprintf("%s %s", firstName, lastName)
							}
							_, err = db.Exec(`INSERT INTO users (userID, userName, cardID, coffeeTotal) VALUES (?, ?, ?, 0)`, userID, userName, cardID)
							if err != nil {
								log.Println(err)
								return
							}
							log.Println("Inserted new user:", userName)
						} else {
							// User exists, update the card number
							_, err = db.Exec(`UPDATE users SET cardID = ? WHERE userID = ?`, cardID, userID)
							if err != nil {
								log.Println(err)
								return
							}
						}
						msg := tgbotapi.NewMessage(userID, fmt.Sprintf("Your card number has been set to %s.", cardID))
						msg.ReplyMarkup = keyboard
						bot.Send(msg)
						continue
					}
				}
				_, err = db.Exec(`INSERT OR IGNORE INTO users (userID, cardID) VALUES (?, ?)`, userID, 0)
				msg := tgbotapi.NewMessage(userID, "Please reply to this message with your Pret a Manger card number. It can be found under the QR code in the Pret App")
				msg.ReplyMarkup = keyboard
				bot.Send(msg)
			}	else {
                    switch update.Message.Text {
					case "Ask for coffee":
						var cardID string
						err := db.QueryRow("SELECT cardID FROM users WHERE userID = ?", userID).Scan(&cardID)
						if err != nil {
							log.Println(err)
							return
						}
					
						users, err := getUsersWithSameCard(db, cardID, userID)
						if err != nil {
							log.Println(err)
							return
						}
					
						if len(users) == 1 {
							selectedUserID = users[0].ID
							selectedUserName = users[0].Name
							promptForCoffee(bot, update.Message.Chat.ID)
						} else if len(users) > 1 {
							var userButtons []tgbotapi.KeyboardButton
							for _, user := range users {
								userButtons = append(userButtons, tgbotapi.NewKeyboardButton(user.Name))
							}
					
							var userButtonRows [][]tgbotapi.KeyboardButton
							for _, button := range userButtons {
								userButtonRows = append(userButtonRows, tgbotapi.NewKeyboardButtonRow(button))
							}
							
							userKeyboard := tgbotapi.NewReplyKeyboard(userButtonRows...)
							msg := tgbotapi.NewMessage(update.Message.Chat.ID, "Who do you want to send it to?")
							msg.ReplyMarkup = userKeyboard
							bot.Send(msg)
						} else {
							msg := tgbotapi.NewMessage(update.Message.Chat.ID, "No other users found.")
							bot.Send(msg)
						}					
					case "Latte", "Cappuccino", "Flat White", "Oat", "Coconut", "Regular", "Vanilla", "Caramel", "None", "Chai Latte":
						if selectedUserID != 0 {
							handleCoffeeRequest(bot, update, selectedUserID)
						}
					case "Back", "Cancel":
						resetCoffeeSelection()
						msg := tgbotapi.NewMessage(userID, "Main Menu")
						msg.ReplyMarkup = keyboard
						bot.Send(msg)
					case "Update Pret Number":
						msg := tgbotapi.NewMessage(userID, "Are you sure you want to update your Pret number?")
						msg.ReplyMarkup = confirmationKeyboard
						bot.Send(msg)
					case "Yes, please":
						_, err = db.Exec("UPDATE users SET cardID = 0 WHERE userID = ?", userID)
						msg := tgbotapi.NewMessage(userID, "Please reply to this message with your new Pret a Manger card number.")
						msg.ReplyMarkup = keyboard
						bot.Send(msg)
					case "No, take me back":
						msg := tgbotapi.NewMessage(userID, "Choose your option:")
						msg.ReplyMarkup = keyboard
						bot.Send(msg)
					case "I got a coffee":
						msg := tgbotapi.NewMessage(userID, "Old functionality. Please use new 'My QR code'.")
						msg.ReplyMarkup = keyboard
						bot.Send(msg)
					case "Settings":
						msg := tgbotapi.NewMessage(userID, "Old functionality. Please use new buttons.")
						msg.ReplyMarkup = keyboard
						bot.Send(msg)
					case "How long do I have to wait":
						handleTimeRemaining(bot, userID, db, keyboard)
					case "Statistics":
						handleStatistics(bot, userID, db, keyboard)
					case "I'm going for a coffee":
						handleGoingForCoffee(bot, userID, db, update, keyboard)
					case "My QR code":
						sendOrGenerateQR(bot, update, inlineKeyboard, db)
					case "Share Subscription Code":
						handleShareSubscriptionCode(bot, update, db)				
					default:
						err := db.QueryRow("SELECT cardID FROM users WHERE userID = ?", userID).Scan(&cardID)
					
						if err != nil {
							log.Println(err)
							return
						}
						// Use this block to capture the card number after /start or /update
						if cardID == "0" {
							handleDefault(bot, update, db, keyboard)
						} else {
							if selectedUserID != 0 {
								// If a user has been selected, proceed with coffee selection
								handleCoffeeRequest(bot, update, selectedUserID)
							} else {
								// Check if the message is a user selection
								var selectedID int64
								var selectedName string
								var cardID string
						
								// Get the cardID of the user sending the message
								err := db.QueryRow("SELECT cardID FROM users WHERE userID = ?", userID).Scan(&cardID)
								if err != nil {
									log.Println(err)
									msg := tgbotapi.NewMessage(update.Message.Chat.ID, "Error retrieving your cardID. Please try again.")
									bot.Send(msg)
									return
								}
						
								// Query the database to find the userID by name and cardID
								err = db.QueryRow("SELECT userID, userName FROM users WHERE userName = ? AND cardID = ?", update.Message.Text, cardID).Scan(&selectedID, &selectedName)
								if err == nil {
									selectedUserID = selectedID
									selectedUserName = selectedName
									promptForCoffee(bot, update.Message.Chat.ID)
								} else {
									msg := tgbotapi.NewMessage(update.Message.Chat.ID, "User not found. Please try again.")
									bot.Send(msg)
								}
							}
						}						
					}
			}
		} else if update.CallbackQuery != nil {
			callbackData := update.CallbackQuery.Data
			inlineuserID := int64(update.CallbackQuery.From.ID)
			log.Println("Received inline command. Doing shit")
			switch callbackData {
			case "coffeereceived":
				handleinlineCoffee(bot, inlineuserID, db, update, keyboard)
			}
		} else {
			continue
		}
	}
}

func handleTimeRemaining(bot *tgbotapi.BotAPI, userID int64, db *sql.DB, keyboard tgbotapi.ReplyKeyboardMarkup) {
    // Retrieve the cardID related to the user
    var cardID string
    var coffeeTotal int
	var coffeeAmount int64
	currentDate := time.Now().Format("2006-01-02")
	var cardIDs []string

	rows, err := db.Query("SELECT cardID FROM cards WHERE DATE(lastdate) < ?", currentDate)
	if err != nil {
		log.Println(err)
		return
	}

	for rows.Next() {
		err := rows.Scan(&cardID)
		if err != nil {
			rows.Close()
			log.Println(err)
			return
		}
		cardIDs = append(cardIDs, cardID)
	}
	rows.Close()

	dbMutex.Lock()
	for _, cardID := range cardIDs {
		_, err := db.Exec("UPDATE OR IGNORE cards SET coffeeAmount = 0 WHERE cardID = ?", cardID)
		if err != nil {
			log.Println(err)
			return
		}
	}
	dbMutex.Unlock()

    err = db.QueryRow("SELECT cardID, coffeeTotal FROM users WHERE userID = ?", userID).Scan(&cardID, &coffeeTotal)
    if err != nil {
        if err == sql.ErrNoRows {
            msg := tgbotapi.NewMessage(userID, "Your card ID is not registered.")
            msg.ReplyMarkup = keyboard
            bot.Send(msg)
            return
        }
		log.Println(err)
		return
    }

    // Retrieve the lastDateTime for the card
    var lastDateTime int64
    err = db.QueryRow("SELECT lastDateTime FROM cards WHERE cardID = ?", cardID).Scan(&lastDateTime)
    if err != nil {
        if err == sql.ErrNoRows {
            msg := tgbotapi.NewMessage(userID, "This card ID does not exist.")
            msg.ReplyMarkup = keyboard
            bot.Send(msg)
            return
        }
		log.Println(err)
		return
    }

    // If no coffees were drunk today, send a specific message
    if lastDateTime == 0 {
        msg := tgbotapi.NewMessage(userID, "No coffees drunk today.")
        msg.ReplyMarkup = keyboard
        bot.Send(msg)
        return
    }

    // Calculate the time remaining
    currentTime := time.Now().Unix()
    timePassed := currentTime - lastDateTime
    timeRemaining := 1800 - timePassed  // 1800 seconds = 30 minutes

	err = db.QueryRow("SELECT coffeeAmount FROM cards WHERE cardID = ?", cardID).Scan(&coffeeAmount)
	if err != nil {
		log.Println(err)
		return
	}


    if timeRemaining > 0 {
        mins := timeRemaining / 60
        secs := timeRemaining % 60
		if coffeeAmount == 4{
			msg := tgbotapi.NewMessage(userID, fmt.Sprintf("You have to wait another %d minutes and %d seconds. You have 1 coffee left", mins, secs))
			msg.ReplyMarkup = keyboard
			bot.Send(msg)
		} else {
			msg := tgbotapi.NewMessage(userID, fmt.Sprintf("You have to wait another %d minutes and %d seconds. You have %d coffees left", mins, secs, 5-coffeeAmount))
			msg.ReplyMarkup = keyboard
			bot.Send(msg)
		}
    } else {
		if coffeeAmount == 4{
			msg := tgbotapi.NewMessage(userID, "You can get your coffee right now! You have 1 coffee left")
			msg.ReplyMarkup = keyboard
			bot.Send(msg)
		} else {
			msg := tgbotapi.NewMessage(userID, fmt.Sprintf("You can get your coffee right now! You have %d coffees left", 5-coffeeAmount))
			msg.ReplyMarkup = keyboard
			bot.Send(msg)
		}
    }
}


func handleStatistics(bot *tgbotapi.BotAPI, userID int64, db *sql.DB, keyboard tgbotapi.ReplyKeyboardMarkup) {
	// Retrieve the total number of coffees for the user
	var coffeeTotal int

	err := db.QueryRow("SELECT coffeeTotal FROM users WHERE userID = ?", userID).Scan(&coffeeTotal)
	if err != nil {
		if err == sql.ErrNoRows {
			msg := tgbotapi.NewMessage(userID, "Your ID is not registered.")
			msg.ReplyMarkup = keyboard
			bot.Send(msg)
			return
		}
		log.Println(err)
		return
	}

	// Send a message to the user with their total number of coffees
	msg := tgbotapi.NewMessage(userID, fmt.Sprintf("You have drunk a total of %d coffees.", coffeeTotal))
	msg.ReplyMarkup = keyboard
	bot.Send(msg)
}

func handleDefault(bot *tgbotapi.BotAPI, update tgbotapi.Update, db *sql.DB, keyboard tgbotapi.ReplyKeyboardMarkup) {
	log.Println("Entered handleDefault function") // Debugging log
	userID := update.Message.Chat.ID
	cardID := strings.ToLower(update.Message.Text) // convert the cardID to lower case
	firstName := strings.TrimSpace(update.Message.Chat.FirstName)
	lastName := strings.TrimSpace(update.Message.Chat.LastName)
	userName := firstName
	if lastName != "" {
		userName = fmt.Sprintf("%s %s", firstName, lastName)
	}

	// Insert or update the user information
	_, err := db.Exec(`INSERT OR REPLACE INTO users (userID, userName, cardID, coffeeTotal) VALUES (?, ?, ?, 0)`, userID, userName, cardID)
	if err != nil {
		log.Println("Error while inserting into users:", err)
		return
	}

	log.Println("Inserted/Updated user information successfully") // Debugging log

	// Insert the card information if it does not exist, without updating lastDateTime
	_, err = db.Exec(`INSERT OR IGNORE INTO cards (cardID, coffeeAmount, lastDateTime, sentMessage) VALUES (?, ?, ?, ?)`, cardID, 0, 0, 1)
	if err != nil {
		log.Println(err)
		return
	}

	log.Println("Inserted/Updated card information successfully") // Debugging log

	// Send a confirmation message
	msg := tgbotapi.NewMessage(userID, fmt.Sprintf("Your Pret a Manger card number has been updated to %s.", cardID))
	msg.ReplyMarkup = keyboard
	sentMsg, err := bot.Send(msg)
	if err != nil {
		log.Println("Failed to send message:", err)
	} else {
		log.Println("Sent message:", sentMsg.Text)
	}
}


func notifyOtherUsers(bot *tgbotapi.BotAPI, cardID string, db *sql.DB, keyboard tgbotapi.ReplyKeyboardMarkup) {
	rows, err := db.Query("SELECT userID FROM users WHERE cardID = ?", cardID)
	if err != nil {
		log.Println(err)
		return
	}
	defer rows.Close()

	for rows.Next() {
		var userID int64
		err := rows.Scan(&userID)
		if err != nil {
			log.Println(err)
			return
		}

		msg := tgbotapi.NewMessage(userID, "You can now get your next coffee!")
		msg.ReplyMarkup = keyboard
		bot.Send(msg)
	}
}

func handleGoingForCoffee(bot *tgbotapi.BotAPI, userID int64, db *sql.DB, update tgbotapi.Update, keyboard tgbotapi.ReplyKeyboardMarkup) {
	var coffeeAmount int64
	var cardID string
	var lastNotification sql.NullInt64
	currentTime := time.Now().Unix()
	currentDate := time.Now().Format("2006-01-02")
	var cardIDs []string

	rows, err := db.Query("SELECT cardID FROM cards WHERE DATE(lastdate) < ?", currentDate)
	if err != nil {
		log.Println(err)
		return
	}

	for rows.Next() {
		err := rows.Scan(&cardID)
		if err != nil {
			rows.Close()
			log.Println(err)
			return
		}
		cardIDs = append(cardIDs, cardID)
	}
	rows.Close()

	dbMutex.Lock()
	for _, cardID := range cardIDs {
		_, err := db.Exec("UPDATE OR IGNORE cards SET coffeeAmount = 0 WHERE cardID = ?", cardID)
		if err != nil {
			log.Println(err)
			return
		}
	}
	dbMutex.Unlock()

	err = db.QueryRow("SELECT cardID FROM users WHERE userID = ?", userID).Scan(&cardID)
	if err != nil {
		log.Println(err)
		return
	}
	
	rows, err = db.Query("SELECT userID FROM users WHERE cardID = ? AND userID != ?", cardID, userID)
	if err != nil {
		log.Println(err)
		return
	}
	defer rows.Close()

	// Fetch the name and surname from the Update object
	userName := update.Message.Chat.FirstName + " " + update.Message.Chat.LastName

	err = db.QueryRow("SELECT coffeeAmount FROM cards WHERE cardID = ?", cardID).Scan(&coffeeAmount)
	if err != nil {
		log.Println(err)
		return
	}
	// Check that the user has not sent the notification for the last 20 mins

	err = db.QueryRow("SELECT lastNotification FROM users WHERE userID = ?", userID).Scan(&lastNotification)
	if err != nil {
		if err == sql.ErrNoRows || !lastNotification.Valid {
			lastNotification.Int64 = 0
		} else {
			log.Println(err)
			return
		}
	}

	if (currentTime-lastNotification.Int64 < 600){
		msg := tgbotapi.NewMessage(userID, "Please don't spam. People were already notified")
		msg.ReplyMarkup = keyboard
		bot.Send(msg)
		return
	}

	_, err = db.Exec("UPDATE users SET lastNotification = ? WHERE userID = ?", currentTime, userID)
	if err != nil {
		log.Println(err)
		return
	}

	for rows.Next() {
		var otherUserID int64
		err := rows.Scan(&otherUserID)
		if err != nil {
			log.Println(err)
			return
		}

		// Now the message will also include the person's name who got the coffee
		if coffeeAmount == 4 {
			msg := tgbotapi.NewMessage(otherUserID, fmt.Sprintf("%s is going for a coffee. Please respect their desire. You have 1 coffee left", userName))
			msg.ReplyMarkup = keyboard
			bot.Send(msg)
		} else {
			msg := tgbotapi.NewMessage(otherUserID, fmt.Sprintf("%s is going for a coffee. Please respect their desire. You have %d coffees left", userName, 5-coffeeAmount))
			msg.ReplyMarkup = keyboard
			bot.Send(msg)
		}
	}
	
	if coffeeAmount == 4 {
		msg := tgbotapi.NewMessage(userID, "I have told everyone that you're going for a coffee. You have 1 coffee left")
		msg.ReplyMarkup = keyboard
		bot.Send(msg)
	} else {
		msg := tgbotapi.NewMessage(userID, fmt.Sprintf("I have told everyone that you're going for a coffee. You have %d coffees left", 5-coffeeAmount))
		msg.ReplyMarkup = keyboard
		bot.Send(msg)
	}
}

func sendOrGenerateQR(bot *tgbotapi.BotAPI, update tgbotapi.Update, inlinekeyboard tgbotapi.InlineKeyboardMarkup, db *sql.DB) {
    defer func() {
        if r := recover(); r != nil {
            log.Printf("Recovered from panic in sendOrGenerateQR: %v", r)
        }
    }()

    var cardID string
    userID := update.Message.Chat.ID
    err := db.QueryRow("SELECT cardID FROM users WHERE userID = ?", userID).Scan(&cardID)
    if err != nil {
        log.Printf("Error retrieving cardID: %v", err)
        return
    }
    sanitizedCardID := sanitizeFilename(cardID)
    filePath := fmt.Sprintf("./cards/%s.png", sanitizedCardID)

    // Check if file exists
    if _, err := os.Stat(filePath); os.IsNotExist(err) {
        // File doesn't exist, create QR code
        qr, err := qrcode.New(strings.ToUpper(sanitizedCardID), qrcode.Medium)
        if err != nil {
            log.Printf("Error generating QR code: %v", err)
            msg := tgbotapi.NewMessage(userID, "Failed to generate QR code.")
            bot.Send(msg)
            return
        }

        // Create a new image with the desired size
        img := image.NewRGBA(image.Rect(0, 0, 400, 700))
        // Fill the image with the background color
        bgColor := color.RGBA{240, 240, 240, 255} // Off-white background color
        draw.Draw(img, img.Bounds(), &image.Uniform{C: bgColor}, image.Point{}, draw.Src)

        // Calculate the size and position of the QR code
        qrSize := 100
        qrPos := image.Point{100, 20}

        // Draw the QR code using red dots
        dotSize := 2
        dotColor := color.RGBA{255, 0, 0, 255} // Red color for dots
        qrImg := qr.Image(qrSize)
        for x := 0; x < qrSize; x++ {
            for y := 0; y < qrSize; y++ {
                if qrImg.At(x, y) == color.Black {
                    dotX := qrPos.X + x*dotSize + dotSize/2
                    dotY := qrPos.Y + y*dotSize + dotSize/2
                    draw.Draw(img, image.Rect(dotX, dotY, dotX+1, dotY+1), &image.Uniform{C: dotColor}, image.Point{}, draw.Src)
                }
            }
        }

        // Create a new drawing context
        dc := gg.NewContextForRGBA(img)

        // Load the font
        err = dc.LoadFontFace("./Whisper.ttf", 18)
        if err != nil {
            log.Printf("Error loading font: %v", err)
            msg := tgbotapi.NewMessage(userID, "Failed to load font.")
            bot.Send(msg)
            return
        }

        // Draw additional information below the QR code
        err = dc.LoadFontFace("./Whisper.ttf", 14)
        if err != nil {
            log.Printf("Error loading font: %v", err)
            msg := tgbotapi.NewMessage(userID, "Failed to load font.")
            bot.Send(msg)
            return
        }
        dc.SetColor(color.Black)
        dc.DrawString("Club Pret subscription", 20, 580)
        dc.DrawString("4/10 stars", 20, 600)
        dc.DrawString("0 perks", 280, 600)

        // Draw the bottom menu bar
        dc.SetColor(color.White)
        dc.DrawRectangle(0, 640, 500, 60)
        dc.Fill()

        // Draw menu icons
        // You can replace these with actual icons or emoji
        dc.SetColor(color.Black)
        dc.DrawString("Home", 50, 670)
        dc.DrawString("Menu", 150, 670)
        dc.DrawString("Wallet", 250, 670)
        dc.DrawString("Account", 350, 670)

        // Save the image as PNG
        err = dc.SavePNG(filePath)
        if err != nil {
            log.Printf("Error saving QR code: %v", err)
            msg := tgbotapi.NewMessage(userID, "Failed to save QR code.")
            bot.Send(msg)
            return
        }
    }

    // Send the QR code image
    msg := tgbotapi.NewPhotoUpload(userID, filePath)
    msg.ReplyMarkup = inlinekeyboard
    bot.Send(msg)
	msg2 := tgbotapi.NewMessage(userID, "")
	msg2.ReplyMarkup = keyboard
	bot.Send(msg2)
}

func handleinlineCoffee(bot *tgbotapi.BotAPI, userID int64, db *sql.DB, update tgbotapi.Update, keyboard tgbotapi.ReplyKeyboardMarkup) {
	// Retrieve the cardID related to the user
	var cardID string
	var lastDateTime int64
	var coffeeAmount int64
	currentDate := time.Now().Format("2006-01-02")
	var cardIDs []string

	rows, err := db.Query("SELECT cardID FROM cards WHERE DATE(lastdate) < ?", currentDate)
	if err != nil {
		log.Println(err)
		return
	}

	for rows.Next() {
		err := rows.Scan(&cardID)
		if err != nil {
			rows.Close()
			log.Println(err)
			return
		}
		cardIDs = append(cardIDs, cardID)
	}
	rows.Close()

	dbMutex.Lock()
	for _, cardID := range cardIDs {
		_, err := db.Exec("UPDATE OR IGNORE cards SET coffeeAmount = 0 WHERE cardID = ?", cardID)
		if err != nil {
			log.Println(err)
			return
		}
	}
	dbMutex.Unlock()

	err = db.QueryRow("SELECT u.cardID, c.lastDateTime, c.coffeeAmount FROM users AS u INNER JOIN cards AS c ON u.cardID = c.cardID WHERE userID = ?", userID).Scan(&cardID, &lastDateTime, &coffeeAmount)
	if err != nil {
		if err == sql.ErrNoRows {
			callbackConfig := tgbotapi.NewCallback(update.CallbackQuery.ID, "You cardID is not registered")
			bot.AnswerCallbackQuery(callbackConfig)
			return
		}
		log.Println(err)
		return
	}

	// Check if 30 minutes have passed since the last coffee
	currentTime := time.Now().Unix()
	timePassed := currentTime - lastDateTime
	if timePassed < 1800 { // 1800 seconds = 30 minutes
		mins := (1800 - timePassed) / 60
		secs := (1800 - timePassed) % 60
		callbackConfig := tgbotapi.NewCallback(update.CallbackQuery.ID, fmt.Sprintf("You couldn't have gotten a coffee. You have to wait for another %d minutes and %d seconds.", mins, secs))
		bot.AnswerCallbackQuery(callbackConfig)
		return
	}

	if coffeeAmount == 5 {
		callbackConfig := tgbotapi.NewCallback(update.CallbackQuery.ID, "You already got your 5 coffees today")
		bot.AnswerCallbackQuery(callbackConfig)
		return
	}	

	// Update coffeeAmount and lastDateTime for the card
	_, err = db.Exec("UPDATE cards SET coffeeAmount = coffeeAmount + 1, lastDateTime = ?, sentMessage = ?, lastdate = ? WHERE cardID = ?", currentTime, 0, currentDate, cardID)
	if err != nil {
		log.Println(err)
		return
	}

	// Update coffeeTotal for the user
	_, err = db.Exec("UPDATE users SET coffeeTotal = coffeeTotal + 1 WHERE userID = ?", userID)
	if err != nil {
		log.Println(err)
		return
	}
	// Notify other users sharing the same card
	err = db.QueryRow("SELECT coffeeAmount FROM cards WHERE cardID = ?", cardID).Scan(&coffeeAmount)
	if err != nil {
		log.Println(err)
		return
	}


	// Notify the user who got the coffee
	if coffeeAmount == 4{
		callbackConfig := tgbotapi.NewCallback(update.CallbackQuery.ID, "Great! I've told everyone that you got a coffee. Enjoy it!. You have 1 coffee left")
		bot.AnswerCallbackQuery(callbackConfig)
	} else {
		callbackConfig := tgbotapi.NewCallback(update.CallbackQuery.ID, fmt.Sprintf("Great! I've told everyone that you got a coffee. Enjoy it!. You have %d coffees left", 5-coffeeAmount))
		bot.AnswerCallbackQuery(callbackConfig)
	}


	rows, err = db.Query("SELECT userID FROM users WHERE cardID = ? AND userID != ?", cardID, userID)
	if err != nil {
		log.Println(err)
		return
	}
	defer rows.Close()

	// Fetch the name and surname from the Update object
	userName := update.CallbackQuery.From.FirstName + " " + update.CallbackQuery.From.LastName

	for rows.Next() {
		var otherUserID int64
		err := rows.Scan(&otherUserID)
		if err != nil {
			log.Println(err)
			return
		}

		// Now the message will also include the person's name who got the coffee
		if coffeeAmount == 4{
			msg := tgbotapi.NewMessage(otherUserID, fmt.Sprintf("%s got a coffee. You have 1 coffee left. 30 mins remaining.", userName))
			msg.ReplyMarkup = keyboard
			bot.Send(msg)
		} else {
			msg := tgbotapi.NewMessage(otherUserID, fmt.Sprintf("%s got a coffee. You have %d coffees left. 30 mins remaining.", userName, 5-coffeeAmount))
			msg.ReplyMarkup = keyboard
			bot.Send(msg)
		}
	}
}

func sanitizeFilename(input string) string {
    return strings.Map(func(r rune) rune {
        if unicode.IsLetter(r) || unicode.IsNumber(r) {
            return r
        }
        return '_'
    }, input)
}

func handleShareSubscriptionCode(bot *tgbotapi.BotAPI, update tgbotapi.Update, db *sql.DB) {
    userID := update.Message.Chat.ID
    var cardID string

    err := db.QueryRow("SELECT cardID FROM users WHERE userID = ?", userID).Scan(&cardID)
    if err != nil {
        if err == sql.ErrNoRows {
            msg := tgbotapi.NewMessage(userID, "Your card ID is not registered.")
            bot.Send(msg)
            return
        }
        log.Println(err)
        return
    }

    // Generate the referral link
    referralLink := fmt.Sprintf("https://t.me/pret_notifier_bot?start=%s", cardID)

    // Send the referral link to the user
    msg := tgbotapi.NewMessage(userID, fmt.Sprintf("Your referral link is: %s", referralLink))
    msg.ReplyMarkup = keyboard
    bot.Send(msg)
}

func getUsersWithSameCard(db *sql.DB, cardID string, excludeUserID int64) ([]User, error) {
    rows, err := db.Query("SELECT userID, userName FROM users WHERE cardID = ? AND userID != ?", cardID, excludeUserID)
    if err != nil {
        return nil, err
    }
    defer rows.Close()

    var users []User
    for rows.Next() {
        var user User
        err := rows.Scan(&user.ID, &user.Name)
        if err != nil {
            return nil, err
        }
        users = append(users, user)
    }

    return users, nil
}

type User struct {
    ID   int64
    Name string
}

func promptForCoffee(bot *tgbotapi.BotAPI, chatID int64) {
    msg := tgbotapi.NewMessage(chatID, "Please choose your coffee:")
    coffeeKeyboard := tgbotapi.NewReplyKeyboard(
        tgbotapi.NewKeyboardButtonRow(
            tgbotapi.NewKeyboardButton("Latte"),
            tgbotapi.NewKeyboardButton("Cappuccino"),
        ),
        tgbotapi.NewKeyboardButtonRow(
            tgbotapi.NewKeyboardButton("Flat White"),
            tgbotapi.NewKeyboardButton("Chai Latte"),
            tgbotapi.NewKeyboardButton("Matcha Latte"),
        ),
        tgbotapi.NewKeyboardButtonRow(
            tgbotapi.NewKeyboardButton("Cancel"),
        ),
    )
    msg.ReplyMarkup = coffeeKeyboard
    bot.Send(msg)
}

func handleCoffeeRequest(bot *tgbotapi.BotAPI, update tgbotapi.Update, recipientID int64) {
    switch update.Message.Text {
    case "Chai Latte", "Latte", "Flat White", "Matcha Latte":
        coffee = update.Message.Text
        promptForIce(bot, update.Message.Chat.ID)
    case "Cappuccino":
        coffee = update.Message.Text
        promptForMilk(bot, update.Message.Chat.ID)
    case "Oat", "Coconut", "Regular", "Soy":
        milk = update.Message.Text
        if coffee == "Matcha Latte" {
            sendCoffeeRequest(bot, update, recipientID)
        } else {
            promptForSyrup(bot, update.Message.Chat.ID)
        }
    case "Vanilla", "Caramel", "None":
        syrup = update.Message.Text
        if syrup == "None" {
            syrup = ""
        } else {
            syrup = " and " + syrup + " syrup"
        }
        sendCoffeeRequest(bot, update, recipientID)
    case "With Ice", "No Ice":
        ice = update.Message.Text
        if coffee == "Chai Latte" || coffee == "Latte" || coffee == "Flat White" || coffee == "Matcha Latte" {
            promptForMilk(bot, update.Message.Chat.ID)
        } else {
            sendCoffeeRequest(bot, update, recipientID)
        }
    }
}

func promptForMilk(bot *tgbotapi.BotAPI, chatID int64) {
    msg := tgbotapi.NewMessage(chatID, "Please choose your milk:")
    milkKeyboard := tgbotapi.NewReplyKeyboard(
        tgbotapi.NewKeyboardButtonRow(
            tgbotapi.NewKeyboardButton("Oat"),
            tgbotapi.NewKeyboardButton("Coconut"),
            tgbotapi.NewKeyboardButton("Regular"),
            tgbotapi.NewKeyboardButton("Soy"),
        ),
        tgbotapi.NewKeyboardButtonRow(
            tgbotapi.NewKeyboardButton("Cancel"),
        ),
    )
    msg.ReplyMarkup = milkKeyboard
    bot.Send(msg)
}

func promptForSyrup(bot *tgbotapi.BotAPI, chatID int64) {
    msg := tgbotapi.NewMessage(chatID, "Please choose your syrup:")
    syrupKeyboard := tgbotapi.NewReplyKeyboard(
        tgbotapi.NewKeyboardButtonRow(
            tgbotapi.NewKeyboardButton("Vanilla"),
            tgbotapi.NewKeyboardButton("Caramel"),
            tgbotapi.NewKeyboardButton("None"),
        ),
        tgbotapi.NewKeyboardButtonRow(
            tgbotapi.NewKeyboardButton("Cancel"),
        ),
    )
    msg.ReplyMarkup = syrupKeyboard
    bot.Send(msg)
}

func promptForIce(bot *tgbotapi.BotAPI, chatID int64) {
    msg := tgbotapi.NewMessage(chatID, "Would you like ice with your coffee?")
    iceKeyboard := tgbotapi.NewReplyKeyboard(
        tgbotapi.NewKeyboardButtonRow(
            tgbotapi.NewKeyboardButton("With Ice"),
            tgbotapi.NewKeyboardButton("No Ice"),
        ),
        tgbotapi.NewKeyboardButtonRow(
            tgbotapi.NewKeyboardButton("Cancel"),
        ),
    )
    msg.ReplyMarkup = iceKeyboard
    bot.Send(msg)
}

func sendCoffeeRequest(bot *tgbotapi.BotAPI, update tgbotapi.Update, recipientID int64) {
    msgText := fmt.Sprintf("%s asked you to get a %s", update.Message.Chat.FirstName+" "+update.Message.Chat.LastName, coffee)
    if coffee != "Matcha Latte" {
        msgText += fmt.Sprintf(" with %s milk%s", milk, syrup)
    }
    if ice == "With Ice" {
        msgText += " with ice"
    }
    msg := tgbotapi.NewMessage(recipientID, msgText)
    bot.Send(msg)
    msg = tgbotapi.NewMessage(update.Message.Chat.ID, "Your coffee request has been sent!")
    msg.ReplyMarkup = keyboard
    bot.Send(msg)
    resetCoffeeSelection()
}

func resetCoffeeSelection() {
    coffee = ""
    milk = ""
    syrup = ""
    ice = ""
    selectedUserID = 0
    selectedUserName = ""
}